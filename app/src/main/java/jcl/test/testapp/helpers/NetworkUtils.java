package jcl.test.testapp.helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by jayanthony.lumba on 4/14/2016.
 */
public class NetworkUtils {
    public static boolean isConnected(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] allNetworks = manager.getAllNetworkInfo();

        for (NetworkInfo networkInfo : allNetworks) {
            if (networkInfo.isAvailable() && networkInfo.isConnected()) {
                return true;
            }
        }
        return false;
    }
}
