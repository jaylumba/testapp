package jcl.test.testapp.helpers;

/**
 * Created by jayanthony.lumba on 4/14/2016.
 */
public class AppUtils {

    public static boolean isLowerThanLollipop(){
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP){
            return false;
        }
        return true;
    }

}
