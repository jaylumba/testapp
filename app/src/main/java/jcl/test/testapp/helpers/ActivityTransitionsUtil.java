package jcl.test.testapp.helpers;

import android.app.Activity;
import android.content.Intent;

import jcl.test.testapp.R;

/**
 * Created by jayanthony.lumba on 4/14/2016.
 */
public class ActivityTransitionsUtil {
    public static void start(Activity activity, Class<?> cls, boolean endActivity){
        Intent i = new Intent(activity, cls);
        activity.startActivity(i);

        if(endActivity)
            activity.finish();
    }

    public static void startWithAnim(Activity activity, Class<?> cls, boolean endActivity){
        Intent i = new Intent(activity, cls);
        activity.startActivity(i);
        activity.overridePendingTransition(R.anim.open_next, R.anim.close_main);
        if(endActivity)
            activity.finish();
    }
}
