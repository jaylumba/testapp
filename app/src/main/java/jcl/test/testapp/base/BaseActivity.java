package jcl.test.testapp.base;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import jcl.test.testapp.R;
import jcl.test.testapp.helpers.AppUtils;

/**
 * Created by jayanthony.lumba on 4/14/2016.
 */
public class BaseActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    Toast toast;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initStatusBar();
        initProgressDialog();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void initStatusBar(){
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (!AppUtils.isLowerThanLollipop()){
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
    }

    private void initProgressDialog(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
    }

    public void showProgressDialog(String message){
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    public void showProgressDialog(int id){
        progressDialog.setMessage(getStringFromResource(id));
        progressDialog.show();
    }

    public void hideProgressDialog(){
        progressDialog.hide();
    }

    protected void showToastMessage(String message){
        if(toast != null){
            toast.cancel();
        }
        toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

    protected void showToastMessage(int stringId){
        if(toast != null){
            toast.cancel();
        }
        toast = Toast.makeText(getApplicationContext(), getStringFromResource(stringId),Toast.LENGTH_SHORT);
        toast.show();
    }

    protected String getStringFromResource(int stringId){
        return getResources().getString(stringId);
    }
}
