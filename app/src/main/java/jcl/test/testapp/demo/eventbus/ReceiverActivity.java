package jcl.test.testapp.demo.eventbus;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;
import butterknife.ButterKnife;
import jcl.test.testapp.R;
import jcl.test.testapp.demo.eventbus.eventmodels.PassPersonalDetailsEvent;

public class ReceiverActivity extends AppCompatActivity {

    @Bind(R.id.lbl_name)TextView lblName;
    @Bind(R.id.lbl_gender) TextView lblGender;
    @Bind(R.id.lbl_age) TextView lblAge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receiver);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onPassPersonalDetailsEvent(PassPersonalDetailsEvent event){
        lblName.setText(event.getName());
        lblGender.setText(event.getGender());
        lblAge.setText(String.valueOf(event.getAge()));
        EventBus.getDefault().removeStickyEvent(event);
    }
}
