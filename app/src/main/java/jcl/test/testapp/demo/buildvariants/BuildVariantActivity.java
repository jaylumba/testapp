package jcl.test.testapp.demo.buildvariants;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import jcl.test.testapp.BuildConfig;
import jcl.test.testapp.R;

public class BuildVariantActivity extends AppCompatActivity {

    @Bind(R.id.lbl_package_name)        TextView lblPackageName;
    @Bind(R.id.lbl_build_config_string) TextView lblBuildConfigString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_build_variant);
        ButterKnife.bind(this);

        lblPackageName.setText(BuildConfig.APPLICATION_ID);
        lblBuildConfigString.setText(BuildConfig.FLAVOR_NAME);

    }
}
