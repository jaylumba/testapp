package jcl.test.testapp.demo.butterknife;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.BindDimen;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jcl.test.testapp.R;
import jcl.test.testapp.base.BaseActivity;

public class ButterKnifeActivity extends BaseActivity {

    //Binding Views
    @Bind(R.id.txt_username)
    EditText txtUsername;
    @Bind(R.id.lbl_output)
    TextView lblOutput;

    //Binding string from string resource
    @BindString(R.string.test_string)
    String testString;

    //Binding int from dimens resource
    @BindDimen(R.dimen.activity_horizontal_margin)
    int horizontalMargin;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_butter_knife);
        ButterKnife.bind(this);
    }

    //You can bind event without casting the view
    @OnClick(R.id.btn_login)
    public void login() {
        String oldText = lblOutput.getText().toString();
        lblOutput.setText("Username: " + txtUsername.getText().toString()
                + "\nString resource: " + testString
                + "\nDimens resource: " + horizontalMargin + "\n\n" + oldText);
    }

}
