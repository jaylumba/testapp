package jcl.test.testapp.demo.eventbus;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import org.greenrobot.eventbus.EventBus;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jcl.test.testapp.R;
import jcl.test.testapp.demo.eventbus.eventmodels.PassPersonalDetailsEvent;
import jcl.test.testapp.helpers.ActivityTransitionsUtil;

public class SenderActivity extends AppCompatActivity {

    @Bind(R.id.txt_name)EditText txtName;
    @Bind(R.id.txt_age) EditText txtAge;
    @Bind(R.id.rg_gender)RadioGroup rgGender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sender);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_post_event)
    public void postEvent(){
        PassPersonalDetailsEvent event = new PassPersonalDetailsEvent();
        event.setName(txtName.getText().toString());
        event.setGender(getSelectedItemOnRadioGroup(rgGender));
        event.setAge(txtAge.getText().toString());
        EventBus.getDefault().postSticky(event);
        ActivityTransitionsUtil.startWithAnim(SenderActivity.this, ReceiverActivity.class,false);
    }

    private String getSelectedItemOnRadioGroup(RadioGroup rg){
        int selectedId = rg.getCheckedRadioButtonId();
         RadioButton rb = (RadioButton) findViewById(selectedId);
        return rb != null ? rb.getText().toString() : "";
    }
}
