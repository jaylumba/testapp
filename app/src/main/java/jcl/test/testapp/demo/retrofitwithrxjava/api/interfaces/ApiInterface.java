package jcl.test.testapp.demo.retrofitwithrxjava.api.interfaces;

import java.util.List;

import jcl.test.testapp.demo.retrofitwithrxjava.api.models.Post;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by jayanthony.lumba on 1/27/2016.
 */

public interface ApiInterface {

    @GET("posts")
    Observable<List<Post>> getAllPost();

    @GET("posts")
    Observable<List<Post>> getPostByUserId(@Query("userId") int userId);

    @GET("posts/{userId}")
    Observable<Post> getPostByPostId(@Path("userId") int userId);

}
