package jcl.test.testapp.demo.retrofitwithrxjava;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jcl.test.testapp.R;
import jcl.test.testapp.base.BaseActivity;
import jcl.test.testapp.demo.retrofitwithrxjava.api.RestClient;
import jcl.test.testapp.demo.retrofitwithrxjava.api.interfaces.ApiInterface;
import jcl.test.testapp.demo.retrofitwithrxjava.api.models.Post;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RetrofitActivity extends BaseActivity {

    @Bind(R.id.spn_select_api)
    Spinner spnSelectApi;
    @Bind(R.id.lbl_output)
    TextView lblOutput;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retrofit);
        ButterKnife.bind(this);
        initSpinner();
    }

    private void initSpinner() {
        String[] spinnerArray = {"Get All Posts", "Get Posts By User ID", "Get Post By Post ID"};
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnSelectApi.setAdapter(spinnerArrayAdapter);
    }

    @OnClick(R.id.btn_go)
    public void go(){
        String action = spnSelectApi.getSelectedItem().toString();
        if (action.equals("Get All Posts")){
            getAllPost();
        }else  if (action.equals("Get Posts By User ID")){
            getPostByUserId();
        }else  if (action.equals("Get Post By Post ID")){
            getPostByPostId();
        }else{
            showToastMessage("No action selected.");
        }
    }

    @OnClick(R.id.btn_clear)
    public void clear(){
        lblOutput.setText("");
    }

    private void getAllPost(){
        showProgressDialog("Loading...");
        ApiInterface service = RestClient.getClient();
        Observable<List<Post>> call = service.getAllPost();
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Post>>() {
                    @Override
                    public void onCompleted() {
                        hideProgressDialog();
                    }
                    @Override
                    public void onError(Throwable e) {
                        hideProgressDialog();
                    }
                    @Override
                    public void onNext(List<Post> response) {
                        hideProgressDialog();
                        String  result = "";
                        for (Post p : response) {
                            result = result + p.toString() + "\n";
                        }
                        lblOutput.setText(result);
                    }
                });
    }

    private void getPostByUserId(){
        showProgressDialog("Loading...");
        ApiInterface service = RestClient.getClient();
        Observable<List<Post>> call = service.getPostByUserId(1);
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Post>>() {
                    @Override
                    public void onCompleted() {
                        hideProgressDialog();
                    }
                    @Override
                    public void onError(Throwable e) {
                        hideProgressDialog();
                    }
                    @Override
                    public void onNext(List<Post> response) {
                        hideProgressDialog();
                        String  result = "";
                        for (Post p : response) {
                            result = result + p.toString() + "\n";
                        }
                        lblOutput.setText(result);
                    }
                });
    }

    private void getPostByPostId(){
        showProgressDialog("Loading...");
        ApiInterface service = RestClient.getClient();
        Observable<Post> call = service.getPostByPostId(1);
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Post>() {
                    @Override
                    public void onCompleted() {
                        hideProgressDialog();
                    }
                    @Override
                    public void onError(Throwable e) {
                        hideProgressDialog();
                    }
                    @Override
                    public void onNext(Post response) {
                        hideProgressDialog();
                        lblOutput.setText(response.toString());
                    }
                });
    }
}
