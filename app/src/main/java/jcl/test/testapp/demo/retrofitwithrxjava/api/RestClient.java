package jcl.test.testapp.demo.retrofitwithrxjava.api;

import java.util.concurrent.TimeUnit;

import jcl.test.testapp.demo.retrofitwithrxjava.api.interfaces.ApiInterface;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jayanthony.lumba on 1/27/2016.
 */
public class RestClient {

    private static ApiInterface apiInterface;
    private static String baseUrl = "http://jsonplaceholder.typicode.com";

    private static final int CONNECT_TIMEOUT_MILLIS = 60000;
    private static final int READ_TIMEOUT_MILLIS = 60000;

    public static ApiInterface getClient() {
        if (apiInterface == null) {

            OkHttpClient okClient = new OkHttpClient.Builder()
                    .connectTimeout(CONNECT_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS)
                    .readTimeout(READ_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS)
                    .build();
//            okClient.interceptors().add(new Interceptor() {
//                @Override
//                public Response intercept(Interceptor.Chain chain) throws IOException {
//                    // try the request
//                    Response response = chain.proceed(chain.request());
//
//                    int tryCount = 0;
//                    while (!response.isSuccessful() && tryCount < 3) {
//                        Log.d("intercept", "Request is not successful - " + tryCount);
//                        tryCount++;
//                        // retry the request
//                        response = chain.proceed(chain.request());
//                    }
//                    // otherwise just pass the original response on
//                    return response;
//                }
//            });

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(okClient)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            apiInterface = client.create(ApiInterface.class);
        }
        return apiInterface;
    }

}
