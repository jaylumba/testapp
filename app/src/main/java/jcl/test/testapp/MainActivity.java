package jcl.test.testapp;

import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;
import jcl.test.testapp.base.BaseActivity;
import jcl.test.testapp.demo.buildvariants.BuildVariantActivity;
import jcl.test.testapp.demo.butterknife.ButterKnifeActivity;
import jcl.test.testapp.demo.eventbus.SenderActivity;
import jcl.test.testapp.helpers.ActivityTransitionsUtil;
import jcl.test.testapp.demo.retrofitwithrxjava.RetrofitActivity;

public class MainActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_retrofit_sample)
    public void launchRetrofitSample(){
        ActivityTransitionsUtil.startWithAnim(MainActivity.this, RetrofitActivity.class,false);
    }

    @OnClick(R.id.btn_eventbus_sample)
    public void launchEventBusSample(){
        ActivityTransitionsUtil.startWithAnim(MainActivity.this, SenderActivity.class,false);
    }

    @OnClick(R.id.btn_butterknife_sample)
    public void launchButterKnifeSample(){
        ActivityTransitionsUtil.startWithAnim(MainActivity.this, ButterKnifeActivity.class,false);
    }

    @OnClick(R.id.btn_buildvariant_sample)
    public void launchBuildVariantSample(){
        ActivityTransitionsUtil.startWithAnim(MainActivity.this, BuildVariantActivity.class,false);
    }
}
