package jcl.test.testapp;

import org.junit.Test;

import jcl.test.testapp.demo.retrofitwithrxjava.api.models.Post;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void testPostModel(){
        Post p = new Post();
        p.setId(1);
        p.setUserId(11);
        p.setTitle("title test");
        p.setBody("body test");
        assertEquals(1,p.getId());
        assertEquals(11,p.getUserId());
        assertEquals("title test",p.getTitle());
        assertEquals("body test",p.getBody());
    }

    @Test
    public void testAppUtils(){

    }
}